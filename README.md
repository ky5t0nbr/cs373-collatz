# CS373: Software Engineering Collatz Repo

* Name: Kyston Brown

* EID: kmb6273

* GitLab ID: ky5t0nbr

* HackerRank ID: KBrowntx

* Git SHA: 63ea4694010f6ee84fea05738f36f8aa9b41a3df

* GitLab Pipelines: https://gitlab.com/ky5t0nbr/cs373-collatz/-/pipelines

* Estimated completion time: 10

* Actual completion time: 40

* Comments: This was a great Project and a great learning experience! It really showed how to increase how cleaned up a program can become!
