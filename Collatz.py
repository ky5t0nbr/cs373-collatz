#!/usr/bin/env python3.11

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ----------
# Collatz.py
# ----------

# ------------
# collatz_eval
# ------------

cache: dict[int, int] = {}


def collatz_eval(t: tuple[int, int]) -> tuple[int, int, int]:
    """
    :param t tuple of the range
    :return max cycle length of the range, inclusive
    """
    i, j = t
    assert i > 0
    assert j > 0
    # get base and ending of range values
    v, base, end = 0, min(i, j), max(i, j)
    # h is the mid point
    halfpoint = (end >> 1) + 1
    base = max(base, halfpoint)
    # calculate max cycle length of range
    for n in range(base, end + 1):
        length = cycle_length(n)
        v = max(v, length)
    assert v > 0
    return i, j, v


# recursviely calculates the cycle length of a
# given value and caches all in between values
def cycle_length(i: int) -> int:
    in_val = i
    # if cached return it
    if in_val in cache:
        return cache[in_val]
    # if done cycling return
    if in_val == 1:
        return 1
    # if odd count 2 cycles and continue
    if in_val % 2 != 0:
        in_val = in_val + (in_val >> 1) + 1
        cycles = 2 + cycle_length(in_val)
        # if cycle length not cached, cache it
        if in_val not in cache and in_val < 1000000:
            cache[in_val] = cycles - 2
    # if even count 1 cycle and continue
    else:
        in_val = int(in_val >> 1)
        cycles = 1 + cycle_length(in_val)
        # if cycle length not cached, cache it
        if in_val not in cache and in_val < 1000000:
            cache[in_val] = cycles - 1
    assert cycles > 0
    return cycles
