#!/usr/bin/env python3.11

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ---------------
# test_Collatz.py
# ---------------

# -------
# imports
# -------

import unittest  # main, TestCase

from Collatz import collatz_eval
from Collatz import cycle_length

# ------------
# Test_Collatz
# ------------


class Test_Collatz(unittest.TestCase):
    def test_collatz_0(self) -> None:
        self.assertEqual(collatz_eval((19, 50)), (19, 50, 112))

    def test_collatz_1(self) -> None:
        self.assertEqual(collatz_eval((150, 300)), (150, 300, 128))

    def test_collatz_2(self) -> None:
        self.assertEqual(collatz_eval((300, 210)), (300, 210, 128))

    def test_collatz_3(self) -> None:
        self.assertEqual(collatz_eval((735, 887)), (735, 887, 179))

    def test_collatz_4(self) -> None:
        self.assertEqual(collatz_eval((529, 768)), (529, 768, 171))

    def test_collatz_5(self) -> None:
        self.assertEqual(collatz_eval((328, 827)), (328, 827, 171))

    def test_collatz_6(self) -> None:
        self.assertEqual(collatz_eval((525, 93)), (525, 93, 144))

    def test_collatz_7(self) -> None:
        self.assertEqual(collatz_eval((521, 317)), (521, 317, 144))

    def test_collatz_8(self) -> None:
        self.assertEqual(collatz_eval((279, 340)), (279, 340, 144))

    def test_collatz_9(self) -> None:
        self.assertEqual(collatz_eval((71, 901)), (71, 901, 179))

    def test_collatz_10(self) -> None:
        self.assertEqual(cycle_length(1), 1)

    def test_collatz_11(self) -> None:
        self.assertEqual(cycle_length(7), 17)

    def test_collatz_12(self) -> None:
        self.assertEqual(cycle_length(450), 54)

    def test_collatz_13(self) -> None:
        self.assertEqual(cycle_length(900), 55)

    def test_collatz_14(self) -> None:
        self.assertEqual(cycle_length(999), 50)

    def test_collatz_15(self) -> None:
        self.assertEqual(cycle_length(271), 43)

    def test_collatz_16(self) -> None:
        self.assertEqual(cycle_length(19687547), 90)

    def test_collatz_17(self) -> None:
        self.assertEqual(cycle_length(12345), 51)

    def test_collatz_18(self) -> None:
        self.assertEqual(cycle_length(2023), 157)

    def test_collatz_19(self) -> None:
        self.assertEqual(cycle_length(314), 38)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
